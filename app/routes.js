const { exchangeRates } = require('../src/util.js');




module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		//create if conditions here to check the currency
		


		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'Error': 'Bad request - missing required parameter name'
			})
		}


		if(req.body.name != String ){
			return res.status(400).send({
				'Error': 'Bad request - name should be string'
			})
		}



		if(req.body.name == ''){
			return res.status(400).send({
				'Error': 'Bad request - name should not be empty'
			})
		}



		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'Error': 'Bad request - missing required parameter ex'
			})
		}


		if(req.body.ex == []){
			return res.status(400).send({
				'Error': 'Bad request - ex should be object'
			})
		}


		if(req.body.ex == null){
			return res.status(400).send({
				'Error': 'Bad request - ex should not be empty'
			})
		}


		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'Error': 'Bad request - missing required parameter alias'
			})
		}



		if(req.body.alias != String ){
			return res.status(400).send({
				'Error': 'Bad request - alias should be string'
			})
		}




		if(req.body.alias == null){
			return res.status(400).send({
				'Error': 'Bad request - alias should not be empty'
			})
		}


		if(req.body == {exchangeRates} ){
			return res.status(400).send({
				'Error': 'Bad request - fields are complete but there is a duplicate alias'
			})
		}




		
		if(req.body != {exchangeRates}){
			return res.status(200).send({
				'Message': 'Fields are complete and no duplicates'
			})
		}



		//return status 200 if route is running
		return res.status(200).send({
			'Message': 'Route is running'
		})
	})
}
