const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', (done) => {
	//add the solutions here
	const domain = 'http://localhost:5001'

	//1
	it('POST /currency endpoint', (done)=>{
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
        		'usd': 0.0092,
        		'won': 10.93,
        		'yuan': 0.065
				}
		})
		.end((err,res)=> {
			expect(res.status).to.equal(200)
			done();
		})
	})


	//2
	it('POST /currency endpoint name is missing', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			fullName: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
        		'usd': 0.0092,
        		'won': 10.93,
        		'yuan': 0.065
				}
		})
		.end((err, res)=> {
			expect(res.status).to.equal(400);
			done()
		})
	})

	//3
	it('POST /currency endpoint name is not a string', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 125,
			ex: {
				'peso': 0.47,
        		'usd': 0.0092,
        		'won': 10.93,
        		'yuan': 0.065
				}
		})
		.end((err, res)=> {
			expect(res.status).to.equal(400);
			done()
		})
	})


	//4
	it('POST /currency endpoint name is empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: '',
			ex: {
				'peso': 0.47,
        		'usd': 0.0092,
        		'won': 10.93,
        		'yuan': 0.065
				}
		})
		.end((err, res)=> {
			expect(res.status).to.equal(400);
			done()
		})
	})


	//5
	it('POST /currency endpoint ex is missing', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			fullName: 'Saudi Arabian Riyadh',
			example: {
				'peso': 0.47,
        		'usd': 0.0092,
        		'won': 10.93,
        		'yuan': 0.065
				}
		})
		.end((err, res)=> {
			expect(res.status).to.equal(400);
			done()
		})
	})


	//6
	it('POST /currency endpoint ex is not an object', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			fullName: 'Saudi Arabian Riyadh',
			ex: [
				{'peso': 0.47},
        		{'usd': 0.0092},
        		{'won': 10.93},
        		{'yuan': 0.065}
				]
		})
		.end((err, res)=> {
			expect(res.status).to.equal(400);
			done()
		})
	})


	//7
	it('POST /currency endpoint ex is empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			fullName: 'Saudi Arabian Riyadh',
			ex: null
		})
		.end((err, res)=> {
			expect(res.status).to.equal(400);
			done()
		})
	})



	//8
	it('POST /currency endpoint name is missing', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			nickName: 'riyadh',
			name: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
        		'usd': 0.0092,
        		'won': 10.93,
        		'yuan': 0.065
				}
		})
		.end((err, res)=> {
			expect(res.status).to.equal(400);
			done()
		})
	})


	//9
	it('POST /currency endpoint alias is not a string', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 12345,
			name: "Saudi Arabian Riyadh",
			ex: {
				'peso': 0.47,
        		'usd': 0.0092,
        		'won': 10.93,
        		'yuan': 0.065
				}
		})
		.end((err, res)=> {
			expect(res.status).to.equal(400);
			done()
		})
	})


	//10
	it('POST /currency endpoint alias is empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: null,
			name: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
        		'usd': 0.0092,
        		'won': 10.93,
        		'yuan': 0.065
				}
		})
		.end((err, res)=> {
			expect(res.status).to.equal(400);
			done()
		})
	})



	//11
	it('POST /currency endpoint duplicate data', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
        		'usd': 0.0092,
        		'won': 10.93,
        		'yuan': 0.065
				}
		})
		.end((err, res)=> {
			expect(res.status).to.equal(400);
			done()
		})
	})


	//12
	it('POST /currency endpoint fields are complete and there are no duplicates', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'Arabian',
			name: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
        		'usd': 0.0092,
        		'won': 10.93,
        		'yuan': 0.065
				}
		})
		.end((err, res)=> {
			expect(res.status).to.equal(200);
			done()
		})
	})


	
})



/*
1. Check if post /currency is running
2. Check if post /currency returns status 400 if name is missing
3. Check if post /currency returns status 400 if name is not a string
4. Check if post /currency returns status 400 if name is empty
5. Check if post /currency returns status 400 if ex is missing
6. Check if post /currency returns status 400 if ex is not an object
7. Check if post /currency returns status 400 if ex is empty
8. Check if post /currency returns status 400 if alias is missing
9. Check if post /currency returns status 400 if alias is not an string
10. Check if post /currency returns status 400 if alias is empty
11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
12. Check if post /currency returns status 200 if all fields are complete and there are no duplicates

*/